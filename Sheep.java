import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Sheep Character
 * Behavior
 * - Walks away from Cursor
 * - Runs away from Wolf
 * @author Quaruzuz
 * @version 20162823
 */
public class Sheep extends SmoothMover
{
    public int SheepDetect = 150;
    public int WolfRange = 100;
    public int SheepRange = 50;
    public int SheepFuzzy = 30;
    public int CursorPosX = -1;
    public int CursorPosY = -1;
    public int Distance = 10000;
    public int Border = 25;
    public boolean Setuped = false;
    SmoothMover Predator;
    SmoothMover OtherSheep;
    long LastTChk = 0;
    long LastCChk = 0;
    public void setupSheep()
    {
        int WorldWidth = getWorld().getWidth();
        int WorldHeight = getWorld().getHeight();
        GreenfootImage baseImg = new GreenfootImage("Sheep.png");
        GreenfootImage image = new GreenfootImage(baseImg);
        image.scale((int)WorldWidth / 25, (int)WorldHeight / 25);
        setImage(image);
        Setuped = true;
    }
    public void getMouse()
    {
        MouseInfo Cursor = Greenfoot.getMouseInfo();
        if(Cursor!=null)
        {
            CursorPosX = Cursor.getX();
            CursorPosY = Cursor.getY();
        }
        else
        {
            CursorPosX = -1;
            CursorPosY = -1;
        }
    }
    public void faceSheep()
    {
        getMouse();
        int SheepPosX = getX();
        int SheepPosY = getY();
        if(isTouching(Sheep.class) && LastTChk + 500 < System.currentTimeMillis())
        {
            for(int Scan = 0; Scan < SheepRange; Scan = Scan + 1)
            {
                if(!(getObjectsInRange(Scan, Sheep.class).isEmpty()))
                {
                    OtherSheep = getObjectsInRange(Scan, Sheep.class).get(0);
                    break;
                }
                else
                {
                    OtherSheep = null;
                }
            }
            if(OtherSheep!=null)
            {
                OtherSheep.turn(Fuzzier(45));
                OtherSheep.move(-1.5);
                turn(Fuzzier(135));
                move(2.5);
            }
            LastTChk = System.currentTimeMillis();
        }
        Distance = (int)Math.sqrt((Math.pow(Math.abs(SheepPosX - CursorPosX), 2)) + (Math.pow(Math.abs(SheepPosY - CursorPosY), 2)));
        for(int Scan = 0; Scan < WolfRange; Scan = Scan + 1)
        {
            if(!(getObjectsInRange(Scan, Wolf.class).isEmpty()))
            {
                Predator = getObjectsInRange(Scan, Wolf.class).get(0);
                break;
            }
            else
            {
                Predator = null;
            }
        }
        if(Predator != null && !(isTouching(Sheep.class)))
        {
            turnTowards(Predator.getX() + Fuzzier(20), Predator.getY() + Fuzzier(20));
            move(-1.5);
        }
        if(CursorPosX != -1 && CursorPosY != -1 && Distance < SheepDetect && LastCChk + 200 < System.currentTimeMillis())
        {
            turnTowards(CursorPosX + Fuzzier(SheepFuzzy), CursorPosY + Fuzzier(SheepFuzzy));
            LastCChk = System.currentTimeMillis();
        }
        else
        {
            turn(Fuzzier(2));
        }
        
    }
    public void moveSheep()
    {
        if(Distance <= SheepDetect && !isTouching(Sheep.class) && !isTouching(Fence.class))
        {
            move(-(2.0 - (Distance / (double)SheepDetect)));
        }
        else if(Greenfoot.getRandomNumber(20) < 5 && !isTouching(Sheep.class) && !isTouching(Fence.class))
        {
            turn(Fuzzier(10));
            move(-0.8);
        }
    }
    public void correctSheep()
    {
        int WorldWidth = getWorld().getWidth();
        int WorldHeight = getWorld().getHeight();
        if(getX() < Border)
        {
            setLocation(Border, getY());
            turn(Fuzzier(135));
            move(-1);
        }
        if(getX() > WorldWidth - Border)
        {
            setLocation(WorldWidth - Border, getY());
            turn(Fuzzier(135));
            move(-1);
        }
        if(getY() < Border)
        {
            setLocation(getX(), Border);
            turn(Fuzzier(135));
            move(-1);
        }
        if(getY() > WorldHeight - Border)
        {
            setLocation(getX(), WorldHeight - Border);
            turn(Fuzzier(135));
            move(-1);
        }
        if(isTouching(Fence.class))
        {
            setLocation(getX(), getY());
        }
        if(isTouching(Sheep.class) && LastTChk + 100 < System.currentTimeMillis() && !isTouching(Fence.class))
        {
            for(int Scan = 0; Scan < SheepRange; Scan = Scan + 1)
            {
                if(!(getObjectsInRange(Scan, Sheep.class).isEmpty()))
                {
                    OtherSheep = getObjectsInRange(Scan, Sheep.class).get(0);
                    break;
                }
                else
                {
                    OtherSheep = null;
                }
            }
            if(OtherSheep!=null)
            {
                OtherSheep.turn(Fuzzier(45));
                OtherSheep.move(-1.5);
                turn(Fuzzier(45));
                move(3.5);
            }
            LastTChk = System.currentTimeMillis();
        }
        else if(isTouching(Sheep.class) && !isTouching(Fence.class))
        {
            move(-1.2);
        }
    }
    public int Fuzzier(int Fuzzy)
    {
        int Random = Greenfoot.getRandomNumber(Fuzzy);
        int Sign = Greenfoot.getRandomNumber(2);
        if(Sign == 1)
        {
            return Random;
        }
        else
        {
            return -Random;
        }
    }
    public void act() 
    {
        if(!Setuped)
        {
            setupSheep();
        }
        getMouse();
        faceSheep();
        correctSheep();
        if(Predator == null)
        {
            moveSheep();
        }
    }    
}
