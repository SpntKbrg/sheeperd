import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * BossWolf Character
 * Behavior
 * - Chases/Eats Sheeps
 * - Stays away from Cursor
 * @author (your name) 
 * @version (a version number or a date)
 */
public class BossWolf extends SmoothMover
{
    int MaxRange = 800;
    public int WolfDetect = 0;
    public int CursorPosX = -1;
    public int CursorPosY = -1;
    public int Distance = 10000;
    public int Border = 0;
    Actor Prey;
    public boolean Setuped = false;
    long LastCChk = 0;
    long LastTChk = 0;
    long LastEChk = 0;
    public void setupWolf()
    {
        int WorldWidth = getWorld().getWidth();
        int WorldHeight = getWorld().getHeight();
        GreenfootImage baseImg = new GreenfootImage("Wolf.png");
        GreenfootImage image = new GreenfootImage(baseImg);
        image.scale((int)WorldWidth / 5, (int)WorldHeight / 6);
        setImage(image);
        Setuped = true;
    }
    public void findSheep()
    {
        for(int Scan = 0; Scan < MaxRange; Scan = Scan + 1)
        {
            if(!(getObjectsInRange(Scan, Sheep.class).isEmpty()))
            {
                Prey = getObjectsInRange(Scan, Sheep.class).get(0);
                break;
            }
            else
            {
                Prey = null;
            }
        }
    }
    public void eatSheep()
    {
        if(isTouching(Sheep.class) && Prey != null && LastEChk + 4000 < System.currentTimeMillis())
        {
            World world = getWorld();
            world.removeObject(Prey);
            MyWorld.SheepEaten = MyWorld.SheepEaten + 1;
            LastEChk = System.currentTimeMillis();
            Timer DeadSheep = getObjectsInRange(this.getWorld().getWidth(), Timer.class).get(0);
            DeadSheep.setValue(DeadSheep.getValue() - 30);
        }
    }
    public void faceWolf()
    {
        if(Prey != null && !isTouching(Fence.class))
        {
            turnTowards(Prey.getX(), Prey.getY());
        }
    }
    public void moveWolf()
    {
        if(Prey != null && !isTouching(Fence.class))
        {
            move(0.5);
        }
        else
        {
            turn(Fuzzier(2));
            move(0.4);
        }
    }
    public int Fuzzier(int Fuzzy)
    {
        int Random = Greenfoot.getRandomNumber(Fuzzy);
        int Sign = Greenfoot.getRandomNumber(2);
        if(Sign == 1)
        {
            return Random;
        }
        else
        {
            return -Random;
        }
    }
    public void act() 
    {
        if(!Setuped)
        {
            setupWolf();
        }
        findSheep();
        if(LastEChk + 4000 < System.currentTimeMillis())
        {
            faceWolf();
            moveWolf();
            eatSheep();
        }
    }
}
