import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Timer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Timer extends Counter
{
    public boolean Setuped = false;
    private SimpleTimer timer = new SimpleTimer();
    public void act() 
    {
        if(!Setuped)
        {
            setup();
        }
        if(timer.millisElapsed() >= 1000)
        {
            if(this.getValue() > 0)
            {
                setValue(this.getValue() - 1);
            }
            timer.mark();
        }
    }
    public void setup()
    {
        timer.mark();
        Setuped = true;
    }
}
