import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Sanctuary here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Sanctuary extends Actor
{
    public boolean Prepared = false;
    public void prepare()
    {
        int WorldWidth = getWorld().getWidth();
        int WorldHeight = getWorld().getHeight();
        GreenfootImage baseImg = new GreenfootImage("Sanctuary.png");
        GreenfootImage image = new GreenfootImage(baseImg);
        image.scale((int)WorldWidth / 16 * 2, (int)WorldHeight / 9 * 2);
        setImage(image);
    }
    public void act() 
    {
        if(!Prepared)
        {
            prepare();
        }
        Actor ArrivedSheep = getOneIntersectingObject(Sheep.class);
        if(ArrivedSheep != null)
        {
            ArrivedSheep.getWorld().removeObject(ArrivedSheep);
            SheepSaved SheepScore = (SheepSaved)(getObjectsInRange(getWorld().getWidth(), SheepSaved.class).get(0));
            if(SheepScore != null)
            {
                SheepScore.setValue(SheepScore.getValue() + 1);
            }
        }
    }    
}
