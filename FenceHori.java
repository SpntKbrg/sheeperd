import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class FenceHori here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class FenceHori extends Actor
{
    /**
     * Act - do whatever the FenceHori wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        Actor StuckWolf = getOneIntersectingObject(Wolf.class);
        if(StuckWolf != null)
        {
            StuckWolf.turnTowards(this.getX(), this.getY());
            StuckWolf.move(-2);
        }
        Actor StuckSheep = getOneIntersectingObject(Sheep.class);
        if(StuckSheep != null)
        {
            StuckSheep.turnTowards(this.getX(), this.getY());
            StuckSheep.move(-2);
        }
    }    
}
