import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Wolf Character
 * Behavior
 * - Chases/Eats Sheeps
 * - Stays away from Cursor
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Wolf extends SmoothMover
{
    int MaxRange = 200;
    public int WolfDetect = 100;
    public int CursorPosX = -1;
    public int CursorPosY = -1;
    public int Distance = 10000;
    public int Border = 50;
    Actor Prey;
    public boolean Setuped = false;
    long LastCChk = 0;
    long LastTChk = 0;
    long LastEChk = 0;
    public void setupWolf()
    {
        int WorldWidth = getWorld().getWidth();
        int WorldHeight = getWorld().getHeight();
        GreenfootImage baseImg = new GreenfootImage("Wolf.png");
        GreenfootImage image = new GreenfootImage(baseImg);
        image.scale((int)WorldWidth / 18, (int)WorldHeight / 22);
        setImage(image);
        Setuped = true;
    }
    public void findSheep()
    {
        for(int Scan = 0; Scan < MaxRange; Scan = Scan + 1)
        {
            if(!(getObjectsInRange(Scan, Sheep.class).isEmpty()))
            {
                Prey = getObjectsInRange(Scan, Sheep.class).get(0);
                break;
            }
            else
            {
                Prey = null;
            }
        }
    }
    public void getMouse()
    {
        MouseInfo Cursor = Greenfoot.getMouseInfo();
        if(Cursor!=null)
        {
            CursorPosX = Cursor.getX();
            CursorPosY = Cursor.getY();
        }
        else
        {
            CursorPosX = -1;
            CursorPosY = -1;
        }
    }
    public void correctWolf()
    {
        int WorldWidth = getWorld().getWidth();
        int WorldHeight = getWorld().getHeight();
        if(getX() < Border)
        {
            setLocation(Border, getY());
            turn(Fuzzier(135));
            move(-1);
        }
        if(getX() > WorldWidth - Border)
        {
            setLocation(WorldWidth - Border, getY());
            turn(Fuzzier(135));
            move(-1);
        }
        if(getY() < Border)
        {
            setLocation(getX(), Border);
            turn(Fuzzier(135));
            move(-1);
        }
        if(getY() > WorldHeight - Border)
        {
            setLocation(getX(), WorldHeight - Border);
            turn(Fuzzier(135));
            move(-1);
        }
        if(!isTouching(Fence.class))
        {
            MaxRange = 400;
        }
        if(isTouching(Fence.class))
        {
            setLocation(getX(), getY());
            MaxRange = 0;
            Prey = null;
        }
        else if(isTouching(Wolf.class) && LastTChk + 500 < System.currentTimeMillis() && !isTouching(Fence.class))
        {
            LastTChk = System.currentTimeMillis();
            move(0.5);
            turn(Fuzzier(40));
            move(-1.5);
        }
    }
    public void eatSheep()
    {
        if(isTouching(Sheep.class) && Prey != null && LastEChk + 2000 < System.currentTimeMillis())
        {
            MyWorld world = (MyWorld)getWorld();
            world.removeObject(Prey);
            LastEChk = System.currentTimeMillis();
            world.SheepEaten = world.SheepEaten + 1;
            Timer DeadSheep = getObjectsInRange(this.getWorld().getWidth(), Timer.class).get(0);
            DeadSheep.setValue(DeadSheep.getValue() - 30);
        }
    }
    public void faceWolf()
    {
        if(Prey != null && !isTouching(Fence.class))
        {
            turnTowards(Prey.getX(), Prey.getY());
        }
    }
    public void moveWolf()
    {
        int WolfPosX = getX();
        int WolfPosY = getY();
        Distance = (int)Math.sqrt((Math.pow(Math.abs(WolfPosX - CursorPosX), 2)) + (Math.pow(Math.abs(WolfPosY - CursorPosY), 2)));
        if(CursorPosX != -1 && CursorPosY != -1 && Distance < WolfDetect && !isTouching(Fence.class))
        {
            turnTowards(CursorPosX, CursorPosY);
            LastCChk = System.currentTimeMillis();
            move(-0.5);
        }
        else if(Prey != null && !isTouching(Fence.class))
        {
            move(3);
        }
        else if(!isTouching(Fence.class))
        {
            turn(Fuzzier(2));
            move(0.2);
        }
    }
    public int Fuzzier(int Fuzzy)
    {
        int Random = Greenfoot.getRandomNumber(Fuzzy);
        int Sign = Greenfoot.getRandomNumber(2);
        if(Sign == 1)
        {
            return Random;
        }
        else
        {
            return -Random;
        }
    }
    public void act() 
    {
        if(!Setuped)
        {
            setupWolf();
        }
        getMouse();
        findSheep();
        if(LastEChk + 1000 < System.currentTimeMillis())
        {
            faceWolf();
            moveWolf();
            eatSheep();
        }
        correctWolf();
    }
}
