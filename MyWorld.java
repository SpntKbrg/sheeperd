import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{

    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        super(1000, 600, 1); 
        prepare();
    }
    public int Clear = 1;
    public int Level = 1;
    public int SheepScore = 0;
    int TimeLimit = 300;
    public int TimeScore = 0;
    int totalLevel = 6; //Include Tutorial Level
    public boolean LoseChk = false;
    public boolean Shown = false;
    public boolean StartLevel = false;
    public static int SheepEaten = 0;
    GreenfootSound backgroundMusic;
    Timer timer;
    Label tutorial;
    SheepSaved sheepsaved;
    public void act()
    {
        if((Level > Clear && sheepCount() == 0) || (Greenfoot.isKeyDown("p")))
        {
            Clear = Clear + 1;
            if(sheepsaved.getValue() != 0)
            {
                TimeScore = TimeScore + timer.getValue();
            }
            else if(SheepEaten == 8)
            {
                LoseChk = true;
                Clear = totalLevel;
            }
            SheepScore = SheepScore + sheepsaved.getValue();
            StartLevel = false;
            clearLevel();
        }
        if(Clear >= totalLevel)
        {
            if(!Shown)
            {
                timer.setValue(0);
                sheepsaved.setValue(0);
                showScore();
                Shown = true;
            }
        }
        if(Level == Clear && Level < totalLevel)
        {
            timer.setValue(TimeLimit);
            sheepsaved.setValue(0);
            loadLevel(Level);
            Level = Level + 1;
            tutorial = new Label("Press Enter to Start Level", 30);
            addObject(tutorial,getWidth() / 2, getHeight() / 100 * 95);
            while((!Greenfoot.isKeyDown("enter") && !StartLevel) && Level != 0)
            {
                Greenfoot.delay(1);
            }
            removeObject(tutorial);
            StartLevel = true;
        }
    }
    private int sheepCount()
    {
        return getObjects(Sheep.class).size();
    }
    private void clearLevel()
    {
        SheepEaten = 0;
        while(getObjects(Wolf.class).size() > 0)
        {
            getObjects(Wolf.class).get(0).Prey = null;
            removeObject(getObjects(Wolf.class).get(0));
        }
        while(getObjects(Fence.class).size() > 0)
        {
            removeObject(getObjects(Fence.class).get(0));
        }
        while(getObjects(Sanctuary.class).size() > 0)
        {
            removeObject(getObjects(Sanctuary.class).get(0));
        }
        while(getObjects(BossWolf.class).size() > 0)
        {
            getObjects(BossWolf.class).get(0).Prey = null;
            removeObject(getObjects(BossWolf.class).get(0));
        }
        while(getObjects(Sheep.class).size() > 0)
        {
            getObjects(Sheep.class).get(0).Predator = null;
            removeObject(getObjects(Sheep.class).get(0));
        }
        if(backgroundMusic != null)
        {
            backgroundMusic.stop();
        }
    }
    private void showScore()
    {
        Counter totalTime = new Counter();
        addObject(totalTime,getWidth() / 2,getHeight() / 4);
        totalTime.setValue(TimeScore);
        Counter totalSheep = new Counter();
        addObject(totalSheep,getWidth() / 2,getHeight() * 3 / 4);
        totalSheep.setValue(SheepScore);
        if(LoseChk)
        {
            backgroundMusic = new GreenfootSound("ScoreBad.mp3");
            backgroundMusic.setVolume(15);
            backgroundMusic.play();
        }
        else if(SheepScore < totalLevel * 8)
        {
            backgroundMusic = new GreenfootSound("ScoreOkay.mp3");
            backgroundMusic.setVolume(15);
            backgroundMusic.play();
        }
        else if(SheepScore == totalLevel * 8)
        {
            backgroundMusic = new GreenfootSound("ScoreGood.mp3");
            backgroundMusic.setVolume(15);
            backgroundMusic.play();
        }
        for(int i = 0; i < SheepScore; i = i + 1)
        {
            Sheep sheep = new Sheep();
            addObject(sheep,Greenfoot.getRandomNumber(getWidth()),Greenfoot.getRandomNumber(getHeight()));
        }
    }
    private void loadLevel(int Choose)
    {
        if(Choose == 0)
        {
            backgroundMusic  = new GreenfootSound("BGM0.mp3");
            backgroundMusic.setVolume(15);
            backgroundMusic.play();
            Sheep sheep = new Sheep();
            addObject(sheep,899,220);
            Sheep sheep2 = new Sheep();
            addObject(sheep2,905,281);
            Sheep sheep3 = new Sheep();
            addObject(sheep3,914,348);
            Sheep sheep4 = new Sheep();
            addObject(sheep4,783,242);
            Sheep sheep5 = new Sheep();
            addObject(sheep5,799,323);
            Sheep sheep6 = new Sheep();
            addObject(sheep6,825,408);
            Sheep sheep7 = new Sheep();
            addObject(sheep7,722,282);
            Sheep sheep8 = new Sheep();
            addObject(sheep8,735,363);
            Sanctuary sanctuary = new Sanctuary();
            addObject(sanctuary,122,320);
            Fence fence = new Fence();
            addObject(fence,500,312);            
        }
        if(Choose == 1)
        {
            backgroundMusic  = new GreenfootSound("BGM1.mp3");
            backgroundMusic.setVolume(15);
            backgroundMusic.play();
            Fence fence2 = new Fence();
            addObject(fence2,532,289);
            Fence fence3 = new Fence();
            addObject(fence3,640,397);
            Fence fence4 = new Fence();
            addObject(fence4,532,397);
            Fence fence5 = new Fence();
            addObject(fence5,640,289);
            Fence fence6 = new Fence();
            addObject(fence6,585,235);
            fence6.turn(90);
            Fence fence7 = new Fence();
            addObject(fence7,640,181);
            Fence fence8 = new Fence();
            addObject(fence8,531,181);
            Sanctuary sanctuary = new Sanctuary();
            addObject(sanctuary,164,296);
            Wolf wolf = new Wolf();
            addObject(wolf,594,298);
            Sheep sheep = new Sheep();
            addObject(sheep,906,409);
            Sheep sheep2 = new Sheep();
            addObject(sheep2,914,359);
            Sheep sheep3 = new Sheep();
            addObject(sheep3,917,312);
            Sheep sheep4 = new Sheep();
            addObject(sheep4,928,260);
            Sheep sheep5 = new Sheep();
            addObject(sheep5,856,262);
            Sheep sheep6 = new Sheep();
            addObject(sheep6,855,323);
            Sheep sheep7 = new Sheep();
            addObject(sheep7,946,213);
            Sheep sheep8 = new Sheep();
            addObject(sheep8,972,399);
            backgroundMusic  = new GreenfootSound("BGM1.mp3");
            backgroundMusic.setVolume(15);
            backgroundMusic.play();
            sheep8.setLocation(942,461);
        }
        if(Choose == 2)
        {
            backgroundMusic  = new GreenfootSound("BGM2.mp3");
            backgroundMusic.setVolume(15);
            backgroundMusic.play();
            Sanctuary sanctuary = new Sanctuary();
            addObject(sanctuary,507,282);
            Wolf wolf = new Wolf();
            addObject(wolf,625,283);
            Wolf wolf2 = new Wolf();
            addObject(wolf2,389,299);
            Fence fence = new Fence();
            addObject(fence,266,196);
            Fence fence2 = new Fence();
            addObject(fence2,272,325);
            fence2.setLocation(267,318);
            fence2.setLocation(265,305);
            fence2.setLocation(266,331);
            Fence fence3 = new Fence();
            addObject(fence3,788,198);
            Fence fence4 = new Fence();
            addObject(fence4,796,333);
            fence4.setLocation(788,327);
            Fence fence5 = new Fence();
            addObject(fence5,793,459);
            fence5.setLocation(788,454);
            Fence fence6 = new Fence();
            addObject(fence6,275,461);
            fence.setLocation(269,162);
            fence2.setLocation(268,289);
            fence6.setLocation(267,411);
            fence6.setLocation(268,414);
            sanctuary.setLocation(510,286);
            fence3.setLocation(785,160);
            fence4.setLocation(786,283);
            fence5.setLocation(786,411);
            Sheep sheep = new Sheep();
            addObject(sheep,935,315);
            Sheep sheep2 = new Sheep();
            addObject(sheep2,931,259);
            Sheep sheep3 = new Sheep();
            addObject(sheep3,882,291);
            Sheep sheep4 = new Sheep();
            addObject(sheep4,875,247);
            Sheep sheep5 = new Sheep();
            addObject(sheep5,879,209);
            Sheep sheep6 = new Sheep();
            addObject(sheep6,944,216);
            Sheep sheep7 = new Sheep();
            addObject(sheep7,885,337);
            Sheep sheep8 = new Sheep();
            addObject(sheep8,940,374);
            Fence fence7 = new Fence();
            addObject(fence7,525,464);
            fence7.turn(90);
            Fence fence8 = new Fence();
            addObject(fence8,522,97);
            fence8.turn(90);
            fence7.setLocation(728,466);
            fence7.setLocation(728,460);
            fence7.setLocation(728,464);
            fence8.setLocation(320,468);
            fence8.setLocation(339,470);
            fence7.setLocation(715,467);
            Fence fence9 = new Fence();
            addObject(fence9,721,109);
            Fence fence10 = new Fence();
            addObject(fence10,360,120);
            fence10.turn(90);
            fence9.turn(90);
            fence9.setLocation(710,105);
            fence10.setLocation(337,104);
            wolf.setLocation(595,280);
            sanctuary.setLocation(539,275);
            wolf2.setLocation(442,273);
            sanctuary.setLocation(522,270);
            wolf.setLocation(601,272);
            fence9.setLocation(714,103);
        }
        if(Choose == 3)
        {
            backgroundMusic  = new GreenfootSound("BGM3.mp3");
            backgroundMusic.setVolume(15);
            backgroundMusic.play();
            Wolf wolf = new Wolf();
            addObject(wolf,94,55);
            Wolf wolf2 = new Wolf();
            addObject(wolf2,112,548);
            Wolf wolf3 = new Wolf();
            addObject(wolf3,102,298);
            Fence fence = new Fence();
            addObject(fence,189,531);
            Fence fence2 = new Fence();
            addObject(fence2,198,411);
            fence2.setLocation(188,404);
            Fence fence3 = new Fence();
            addObject(fence3,193,71);
            Fence fence4 = new Fence();
            addObject(fence4,198,205);
            fence4.setLocation(193,197);
            fence4.setLocation(192,190);
            fence4.setLocation(193,195);
            Fence fence5 = new Fence();
            addObject(fence5,620,432);
            Fence fence6 = new Fence();
            addObject(fence6,627,314);
            Fence fence7 = new Fence();
            addObject(fence7,626,176);
            fence6.setLocation(618,302);
            fence6.setLocation(620,301);
            fence7.setLocation(618,177);
            Sanctuary sanctuary = new Sanctuary();
            addObject(sanctuary,942,308);
            sanctuary.setLocation(929,294);
            Fence fence8 = new Fence();
            addObject(fence8,792,82);
            fence8.setLocation(781,74);
            Fence fence9 = new Fence();
            addObject(fence9,817,239);
            fence8.setLocation(843,71);
            fence9.setLocation(844,199);
            sanctuary.setLocation(912,314);
            Fence fence10 = new Fence();
            addObject(fence10,854,438);
            Fence fence11 = new Fence();
            addObject(fence11,854,546);
            fence10.setLocation(850,421);
            fence10.setLocation(848,421);
            fence11.setLocation(848,543);
            Fence fence12 = new Fence();
            addObject(fence12,335,186);
            fence12.setLocation(333,178);
            Fence fence13 = new Fence();
            addObject(fence13,340,310);
            fence13.setLocation(332,310);
            fence13.setLocation(331,301);
            Fence fence14 = new Fence();
            addObject(fence14,339,431);
            fence14.setLocation(334,417);
            Sheep sheep = new Sheep();
            addObject(sheep,451,184);
            Sheep sheep2 = new Sheep();
            addObject(sheep2,525,245);
            Sheep sheep3 = new Sheep();
            addObject(sheep3,430,258);
            Sheep sheep4 = new Sheep();
            addObject(sheep4,541,319);
            Sheep sheep5 = new Sheep();
            addObject(sheep5,459,320);
            Sheep sheep6 = new Sheep();
            addObject(sheep6,550,393);
            Sheep sheep7 = new Sheep();
            addObject(sheep7,393,372);
            Sheep sheep8 = new Sheep();
            addObject(sheep8,473,427);
            wolf.setLocation(100,228);
            wolf2.setLocation(102,355);
            fence4.setLocation(275,178);
            fence2.setLocation(271,360);
            fence4.setLocation(273,237);
            fence4.setLocation(270,236);
            fence12.setLocation(332,173);
            removeObject(fence12);
            removeObject(fence13);
            removeObject(fence14);
            fence.setLocation(134,529);
            fence3.setLocation(130,67);
            fence6.setLocation(682,296);
            fence7.setLocation(624,181);
            fence7.setLocation(681,174);
            fence5.setLocation(681,416);
        }
        if(Choose == 4)
        {
            backgroundMusic  = new GreenfootSound("BGM4.mp3");
            backgroundMusic.setVolume(15);
            backgroundMusic.play();
            Fence fenceH = new Fence();
            addObject(fenceH,494,98);
            fenceH.turn(90);
            Fence fenceH2 = new Fence();
            addObject(fenceH2,626,103);
            fenceH2.setLocation(625,99);
            Fence fenceH3 = new Fence();
            addObject(fenceH3,369,100);
            fenceH3.setLocation(369,96);
            fenceH3.setLocation(370,98);
            Fence fence = new Fence();
            addObject(fence,201,267);
            fence.setLocation(203,259);
            Fence fence2 = new Fence();
            addObject(fence2,211,386);
            fence2.setLocation(202,377);
            fence2.setLocation(203,366);
            Fence fenceH4 = new Fence();
            addObject(fenceH4,495,508);
            Fence fenceH5 = new Fence();
            addObject(fenceH5,385,514);
            fenceH5.setLocation(378,507);
            Fence fenceH6 = new Fence();
            addObject(fenceH6,635,512);
            fenceH4.setLocation(501,505);
            fenceH6.setLocation(625,503);
            Fence fence3 = new Fence();
            addObject(fence3,798,234);
            fence3.setLocation(799,230);
            Fence fence4 = new Fence();
            addObject(fence4,805,359);
            fence4.setLocation(798,352);
            Sanctuary sanctuary = new Sanctuary();
            addObject(sanctuary,506,295);
            Wolf wolf = new Wolf();
            addObject(wolf,408,308);
            Wolf wolf2 = new Wolf();
            addObject(wolf2,603,297);
            fence3.setLocation(680,252);
            fence4.setLocation(682,376);
            fence.setLocation(313,249);
            fence2.setLocation(310,374);
            wolf2.setLocation(880,285);
            sanctuary.setLocation(493,41);
            fenceH3.setLocation(371,176);
            fenceH.setLocation(493,176);
            fenceH2.setLocation(621,179);
            fenceH.setLocation(497,179);
            fenceH3.setLocation(374,178);
            fenceH5.setLocation(359,515);
            fenceH4.setLocation(484,513);
            fenceH6.setLocation(620,512);
            fenceH4.setLocation(497,512);
            fenceH5.setLocation(374,511);
            Fence fence5 = new Fence();
            addObject(fence5,809,462);
            Fence fence6 = new Fence();
            addObject(fence6,222,460);
            Fence fence7 = new Fence();
            addObject(fence7,508,543);
            fenceH4.setLocation(498,510);
            fence7.setLocation(497,562);
            fenceH3.setLocation(367,130);
            fenceH.setLocation(491,130);
            fenceH2.setLocation(613,129);
            fenceH2.setLocation(615,134);
            fence.setLocation(311,197);
            fence2.setLocation(311,318);
            fence3.setLocation(670,199);
            fence4.setLocation(669,318);
            fenceH5.setLocation(364,509);
            fenceH4.setLocation(486,509);
            fenceH6.setLocation(610,510);
            fenceH6.setLocation(608,506);
            fenceH5.setLocation(369,473);
            fenceH4.setLocation(491,472);
            fenceH6.setLocation(609,473);
            fence7.setLocation(491,540);
            fence6.setLocation(187,532);
            fence5.setLocation(813,536);
            Fence fenceH7 = new Fence();
            addObject(fenceH7,743,382);
            Fence fenceH8 = new Fence();
            addObject(fenceH8,245,377);
            wolf.setLocation(239,310);
            removeObject(fence);
            removeObject(fence3);
            Fence fenceH9 = new Fence();
            addObject(fenceH9,385,383);
            fenceH9.setLocation(382,376);
            Fence fenceH10 = new Fence();
            addObject(fenceH10,607,381);
            Fence fenceH11 = new Fence();
            addObject(fenceH11,497,381);
            wolf.setLocation(391,288);
            wolf2.setLocation(611,292);
            Wolf wolf3 = new Wolf();
            addObject(wolf3,490,282);
            fenceH9.setLocation(376,328);
            fenceH11.setLocation(494,330);
            fenceH10.setLocation(607,330);
            fenceH7.setLocation(736,329);
            fenceH8.setLocation(245,335);
            fence2.setLocation(312,271);
            fence4.setLocation(672,308);
            fence2.setLocation(310,313);
            fenceH8.setLocation(244,327);
            Sheep sheep = new Sheep();
            addObject(sheep,618,588);
            sheep.setLocation(538,520);
            Sheep sheep2 = new Sheep();
            addObject(sheep2,612,522);
            Sheep sheep3 = new Sheep();
            addObject(sheep3,691,528);
            Sheep sheep4 = new Sheep();
            addObject(sheep4,768,536);
            Sheep sheep5 = new Sheep();
            addObject(sheep5,449,524);
            Sheep sheep6 = new Sheep();
            addObject(sheep6,382,531);
            Sheep sheep7 = new Sheep();
            addObject(sheep7,311,532);
            Sheep sheep8 = new Sheep();
            addObject(sheep8,238,542);
            sheep8.setLocation(234,520);
            fenceH.turn(90);
            fenceH2.turn(90);
            fenceH3.turn(90);
            fenceH4.turn(90);
            fenceH5.turn(90);
            fenceH6.turn(90);
            fenceH7.turn(90);
            fenceH8.turn(90);
        }
        if(Choose == 5)
        {
            backgroundMusic  = new GreenfootSound("BGM5.mp3");
            backgroundMusic.setVolume(15);
            backgroundMusic.play();
            Fence fence = new Fence();
            addObject(fence,217,174);
            Fence fence2 = new Fence();
            addObject(fence2,217,282);
            Fence fence3 = new Fence();
            addObject(fence3,217,390);
            Fence fence4 = new Fence();
            addObject(fence4,823,162);
            Fence fence5 = new Fence();
            addObject(fence5,823,270);
            Fence fence6 = new Fence();
            addObject(fence6,823,378);
            Fence fence7 = new Fence();
            addObject(fence7,537,65);
            Fence fence8 = new Fence();
            addObject(fence8,540,539);
            Fence fence9 = new Fence();
            addObject(fence9,539,290);
            Sanctuary sanctuary = new Sanctuary();
            addObject(sanctuary,961,280);
            BossWolf bosswolf = new BossWolf();
            addObject(bosswolf,701,289);
            Sheep sheep = new Sheep();
            addObject(sheep,62,217);
            Sheep sheep2 = new Sheep();
            addObject(sheep2,94,276);
            Sheep sheep3 = new Sheep();
            addObject(sheep3,149,224);
            Sheep sheep4 = new Sheep();
            addObject(sheep4,83,335);
            Sheep sheep5 = new Sheep();
            addObject(sheep5,179,294);
            Sheep sheep6 = new Sheep();
            addObject(sheep6,158,372);
            Sheep sheep7 = new Sheep();
            addObject(sheep7,87,414);
            Sheep sheep8 = new Sheep();
            addObject(sheep8,110,163);
        }
    }
    private void prepare()
    {
        timer = new Timer();
        addObject(timer,955,586);
        sheepsaved = new SheepSaved();
        addObject(sheepsaved,955,557);
    }
}
